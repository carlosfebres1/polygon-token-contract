pragma solidity 0.6.6;

import "./ChildERC20.sol";
import "./ApproveAndCallFallBack.sol";

contract MindsChildToken is ChildERC20 {
    constructor(
        string memory name,
        string memory symbol,
        uint8 decimals,
        address childChainManager
    )
    public
    ChildERC20(
        name,
        symbol,
        decimals,
        childChainManager
    )
    {}

    function approveAndCall(
        address _spender,
        uint256 _value,
        bytes memory _extraData
    ) public returns (bool success) {
        _approve(msg.sender, _spender, _value);

        ApproveAndCallFallBack(_spender).receiveApproval(
            msg.sender,
            _value,
            address(this),
            _extraData
        );

        return true;
    }
}