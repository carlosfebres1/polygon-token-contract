pragma solidity 0.6.6;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

import "../ApproveAndCallFallBack.sol";

contract TestERC20 is ERC20 {
    constructor(
        string memory name,
        string memory symbol
    )
    public
    ERC20(name, symbol)
    {
        _mint(msg.sender, 100000 * (10 ** 18));
    }

    function approveAndCall(
        address _spender,
        uint256 _value,
        bytes memory _extraData
    ) public returns (bool success) {
        _approve(msg.sender, _spender, _value);

        ApproveAndCallFallBack(_spender).receiveApproval(
            msg.sender,
            _value,
            address(this),
            _extraData
        );

        return true;
    }
}