task("deploy-child-token", "Deploy a Child Token Contract")
  .addParam("name", "name of the token")
  .addParam("symbol", "symbol of the token")
  .addParam("decimals", "numbers of decimals of the ERC20 token")
  .addParam("childChainManager", "address of the Child Chain Manager")
  .setAction(async (taskArgs, hre) => {
    const MindsChildToken = await hre.ethers.getContractFactory(
      "MindsChildToken"
    );
    const token = await MindsChildToken.deploy(
      taskArgs.name,
      taskArgs.symbol,
      taskArgs.decimals,
      taskArgs.childChainManager
    );

    await token.deployed();

    console.log("MindsChildToken deployed to:", token.address);
  });
