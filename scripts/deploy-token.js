async function main() {
    const ERC20 = await hre.ethers.getContractFactory("TestERC20");
    const token = await ERC20.deploy('Minds Test', 'tMINDS');

    await token.deployed();

    console.log("Token deployed to:", token.address);
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });