async function main() {
    const MindsChildToken = await hre.ethers.getContractFactory("MindsChildToken");
    const token = await MindsChildToken.deploy('Minds Test', 'tMINDS', 18 , "0xb5505a6d998549090530911180f38aC5130101c6");

    await token.deployed();

    console.log("MindsChildToken deployed to:", token.address);
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });