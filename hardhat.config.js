require("@nomiclabs/hardhat-waffle");
require("@nomiclabs/hardhat-etherscan");
require("dotenv").config();

// Import tasks
require("./tasks/deploy-child-token");

const privateKey = process.env.PRIVATE_KEY;
const etherscanApiKey = process.env.ETHERSCAN_API_KEY;

if (!privateKey)
  throw new Error('Could not find Private Key');

module.exports = {
  solidity: "0.6.6",
  networks: {
    hardhat: {
    },
    polygon: {
      url: "https://rpc-mainnet.maticvigil.com/",
      accounts: [privateKey]
    },
    mumbai: {
      url: "https://rpc-mumbai.maticvigil.com/",
      accounts: [privateKey]
    },
    goerli: {
      url: "https://goerli.infura.io/v3/9aa3d95b3bc440fa88ea12eaa4456161",
      accounts: [privateKey],
    }
  },
  etherscan: {
    apiKey: etherscanApiKey
  }
};
