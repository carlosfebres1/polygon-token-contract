# Child Token Contract

## Deployment Process

You can deploy a new instance of the Child Token contract by running the `deploy-child-token` hardhat function.

First, you will need to provide a private key in the `.env` file. (you can duplicate the `.env.sample` file and enter the values).

The task has 4 parameters:

- **name**: Name of the token
- **symbol**: Symbol of the token
- **decimals**: Number of decimals
- **child-chain-manager**: Address of the Child Token Manager

### Example

To deploy, on the Goerli chain, a _Child Token Contract_ of `name` "Minds", `symbol` "MIND", `decimals` "18" and `child-chain-manager` of "0xA6FA4fB5f76172d178d61B04b0ecd319C5d1C0aa".

Run this command:

`hardhat deploy-child-token --name Minds --symbol MIND --decimals 18 --child-chain-manager 0x256B70644f5D77bc8e2bb82C731Ddf747ecb1471 --network goerli`
